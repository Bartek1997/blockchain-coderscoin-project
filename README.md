# Blockchain Coderscoin
It´s a blockchain learing tool, for teaching blockchain. You can generate keys, make transactions, mine blocks and visualize the whole blockchain with it´s forks. Made by a group of 3 passionate developers for CODERS.BAY Vienna to be used in future for teaching purposes.

### Coded by Bartlomiej Padiasek:
- Frontend
    - Error Handling
    - AntV G6 visuals
- Backend
    - mostly done by me

### Used in this project
- Vue.JS (HTML/CSS/JavaScript), SpringBoot (Java), MySQL, AntV-G6 (https://g6.antv.vision/en)


# How to start
- ***MySQL***
- Install ***NodeJS*** & ***NPM***

### Frontend
- Open Project in IDE, run ***npm install*** and ***npm run serve***
- Type in browser ***http://localhost:3000***

### Backend
- Install ***Java 17***
- Open project in ***Intellij***
- Run ***Main.java***
