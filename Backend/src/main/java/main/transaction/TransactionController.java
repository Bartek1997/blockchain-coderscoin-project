package main.transaction;

import main.connectionVisual.ConnectionVisual;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;

@CrossOrigin
@RestController
public class TransactionController {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private TransactionService transactionService;

    @PostMapping("/create-transaction/{privateKey}")
    public Transaction createNewTransaction(@RequestBody Transaction transaction, @PathVariable String privateKey) throws NoSuchAlgorithmException {
        return transactionService.createNewTransaction(transaction, privateKey);
    }

    @GetMapping("/get-list-for-transaction-visualisation")
    public ConnectionVisual getTransForVisualisation() throws NoSuchAlgorithmException {
        return transactionService.getTransactionsForVisualisation ();
    }

    @GetMapping("/user/get-free-starting-coins")
    public Transaction freeCoinsTrans (){
        return transactionService.freeCoinsTransactionAtStart ();
    }

    @GetMapping("/get-transaction-by-signed-transaction/{signedTransaction}")
    public Transaction getBlockById (@PathVariable String signedTransaction){
        return transactionRepository.findBySignedTransaction(signedTransaction);
    }
}
