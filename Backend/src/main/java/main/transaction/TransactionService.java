package main.transaction;

import main.block.Block;
import main.block.BlockService;
import main.keyPair.KeyPrivatePublicController;
import main.keyPair.KeyPrivatePublicDTO;
import main.connectionVisual.ConnectionVisual;
import main.connectionVisual.ConnectionVisualContent;
import main.connectionVisual.ConnectionVisualNode;
import main.util.Utils;
import org.bitcoinj.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.xml.bind.DatatypeConverter;
import java.security.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static main.util.Utils.masterKeyPublic;
import static main.util.Utils.masterKeyPrivate;

@Service
public class TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private BlockService blockService;
    @Autowired
    private KeyPrivatePublicController keyPrivatePublicController;

    public String signedHash(String calculatedHash, String privateKey, Transaction transaction) throws SignatureDecodeException {

        // creating a private key object from private key WIF String
        DumpedPrivateKey dpk = DumpedPrivateKey.fromBase58(null, privateKey);
        ECKey privateKeyToSign = dpk.getKey();

        // creating Sha object from hashed transaction String
        Sha256Hash hashedTransaction = Sha256Hash.wrap(calculatedHash);

        // sign transaction with private key
        ECKey.ECDSASignature singedTransaction = privateKeyToSign.sign(hashedTransaction);

        // encoding to byte
        byte[] res = singedTransaction.encodeToDER();

        // verify transaction
        boolean b = privateKeyToSign.verify(hashedTransaction.getBytes(), ECKey.ECDSASignature.decodeFromDER(res), Base58.decode(transaction.getPublicKeyWithdrawal()));
        if (!b) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "transaction could not verify");
        }

        return DatatypeConverter.printHexBinary(res);

    }

    public void checkBalanceForTransaction(Transaction transaction) throws NoSuchAlgorithmException {
        if(!transaction.getPublicKeyWithdrawal().equals(masterKeyPublic)){
            if(getFundsFromPublicKeyOfTheLongestChain(transaction.getPublicKeyWithdrawal()) < transaction.getAmount()){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not enough funds for transaction available");
            }
        }
    }

    public ConnectionVisual getTransactionsForVisualisation() throws NoSuchAlgorithmException {
        List<ConnectionVisualContent> nodesContentList = new ArrayList<>();
        List<ConnectionVisualNode> nodesIdList = new ArrayList<>();
        List<Transaction> transactionList = new ArrayList<>();
        for (Block block : blockService.getLongestChainFromTheBlockchain()) {
            for (Transaction transaction:block.getTransactionList()) {
                transactionList.add(transaction);
            }
        }

        for (Transaction transaction: transactionList) {
            if (transaction.getSignedTransaction().substring(0,6).equals("Mining")) {
                nodesIdList.add(new ConnectionVisualNode(transaction.getPublicKeyDeposit(), getFundsFromPublicKeyOfTheLongestChain(transaction.getPublicKeyDeposit())));
            }
            else if(transaction.getPublicKeyWithdrawal().equals(masterKeyPublic)){
                nodesIdList.add(new ConnectionVisualNode(transaction.getPublicKeyDeposit(), getFundsFromPublicKeyOfTheLongestChain(transaction.getPublicKeyDeposit())));
            }
            else{
                nodesIdList.add(new ConnectionVisualNode(transaction.getPublicKeyWithdrawal(), getFundsFromPublicKeyOfTheLongestChain(transaction.getPublicKeyWithdrawal())));
                nodesIdList.add(new ConnectionVisualNode(transaction.getPublicKeyDeposit(), getFundsFromPublicKeyOfTheLongestChain(transaction.getPublicKeyDeposit())));
                nodesContentList.add(new ConnectionVisualContent(transaction.getPublicKeyWithdrawal(), transaction.getPublicKeyDeposit(),transaction.getAmount()));
            }
        }

        List<ConnectionVisualNode> nodesIdListEdited = new ArrayList<>();
        for (ConnectionVisualNode nodesId : nodesIdList) {
            if (!nodesIdListEdited.stream().anyMatch(p -> p.getId().equals(nodesId.getId()))) {
                nodesIdListEdited.add(nodesId);
            }
        }

        return new ConnectionVisual(nodesIdListEdited,nodesContentList);
    }

    public Transaction freeCoinsTransactionAtStart() {
        List<KeyPrivatePublicDTO> keyPrivatePublicDTOList = keyPrivatePublicController.getAllKeyPairsByUser();
        Collections.sort(keyPrivatePublicDTOList, new Comparator<KeyPrivatePublicDTO>() {
            @Override
            public int compare(KeyPrivatePublicDTO o1, KeyPrivatePublicDTO o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        String publicKey = keyPrivatePublicDTOList.get(0).getPublicKey();
        Transaction freeCoinsTransaction = new Transaction(masterKeyPublic, publicKey, 10);
        freeCoinsTransaction.setTimestamp(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        String calculateHash = Utils.calculateHash(freeCoinsTransaction);
        try{
            freeCoinsTransaction.setSignedTransaction(signedHash(calculateHash, masterKeyPrivate, freeCoinsTransaction));
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Transaction could not validate", e);
        }
        transactionRepository.save(freeCoinsTransaction);
        return freeCoinsTransaction;
    }

    public void checkIfWithdrawalAndDepositAreTheSame(Transaction transaction){
        if (transaction.getPublicKeyWithdrawal().equals(transaction.getPublicKeyDeposit())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Withdrawal and Deposit Key are the same");
        }
    }

    public void checkIfTransactionSendsZeroCoins(Transaction transaction){
        if (0 >= transaction.getAmount()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Amount is to low for a transaction");

        }
    }

    public double getFundsFromPublicKeyOfTheLongestChain(String publicKey) throws NoSuchAlgorithmException {
        double funds = 0;
        for (Block block : blockService.getLongestChainFromTheBlockchain()) {
            for (Transaction transaction : block.getTransactionList()) {
                if (transaction.getPublicKeyDeposit().equals(publicKey)) {
                    funds = funds + transaction.getAmount();
                }
                if (transaction.getPublicKeyWithdrawal().equals(publicKey)) {
                    funds = funds - transaction.getAmount();
                }
            }
        }
        return funds;
    }

    public Transaction createNewTransaction(Transaction transaction, String privateKey) throws NoSuchAlgorithmException {
        checkIfTransactionSendsZeroCoins(transaction);
        checkIfWithdrawalAndDepositAreTheSame(transaction);
        checkBalanceForTransaction(transaction);
        transaction.setTimestamp(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        String calculatedHash = Utils.calculateHash(transaction);
        try{
            transaction.setSignedTransaction(signedHash(calculatedHash, privateKey, transaction));
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Transaction could not validate", e);
        }
        return transaction;
    }

}
