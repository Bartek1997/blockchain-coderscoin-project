package main.login;

import main.keyPair.KeyPrivatePublicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserCRUDRepository userCRUDRepository;
    @Autowired
    private EntityManager em;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private KeyPrivatePublicService keyPrivatePublicService;

    @RequestMapping(method = RequestMethod.POST, path = "/register")
    /**
     * tries to add new useraccount
     * return true if successfull
     */
    public boolean register(@RequestBody User user) throws NoSuchAlgorithmException {
        boolean registered = userRepository.insertNewUser(user.getUserName(), encoder.encode(user.getPassword()), user.getFirstName(), user.getLastName());
        if(!registered) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "username already exists");
        }
        keyPrivatePublicService.firstKeyPairGenrator(userRepository.findByName(user.getUserName()).getIdUser());
        return true;
    }


    @RequestMapping(method = RequestMethod.GET, path = "/signedInUser")
    /**
    * returns username of signed it use or null if noone is signed in
    */
    public User getSignedInUser(){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null || authentication.getName().equals("anonymousUser")){
            return null;
        } else {
            return userCRUDRepository.findByUserName(authentication.getName()).get();
        }
    }

    @RequestMapping(method = RequestMethod.POST, path = "/logout")
    /**
     * logs user out
     */
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/users")
    public List<User> getAllUsers(){
        var users = userCRUDRepository.findAll();
        List<User> userList = new ArrayList<>();
        var iterator = users.iterator();
        while (iterator.hasNext()){
            User user = iterator.next();
            user.setPassword(null); //we do not want to send the (hashed) pw to the client
            userList.add(user);
        }
        return userList;
    }


}

