package main.login;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserCRUDRepository extends CrudRepository<User, Integer> {
    Optional<User> findByUserName(String userName);
}
