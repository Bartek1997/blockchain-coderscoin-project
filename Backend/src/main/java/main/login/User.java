package main.login;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import main.keyPair.KeyPrivatePublic;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idUser;

    @Column(unique = true)
    private String userName;

    @Column
    private String password;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @OneToMany(mappedBy = "user")
    @JsonManagedReference(value = "keyPrivatePublicList")
    private List<KeyPrivatePublic> keyPrivatePublicList = new ArrayList<>();

    public User() {
    }

    public User(String userName, String password, String firstName, String lastName) {
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<KeyPrivatePublic> getKeyPrivatePublicList() {
        return keyPrivatePublicList;
    }

    public void setKeyPrivatePublicList(List<KeyPrivatePublic> keyPrivatePublicList) {
        this.keyPrivatePublicList = keyPrivatePublicList;
    }
}
