package main.block;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import main.transaction.Transaction;
import main.util.Utils;

import javax.persistence.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static main.util.Utils.genesisBlockHash;

@Entity
public class Block {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idBlock;

    private long idBlockForHash;
    private String previousHash;
    private LocalDateTime timestamp;
    private BigInteger nonce;

    @OneToMany(mappedBy = "block")
    @JsonManagedReference
    private List<Transaction> transactionList = new ArrayList<>();

    @Transient
    private boolean blockCorrectMined;

    public Block() {
    }

    public Block( List<Transaction> transactionList, long idBlockForHash, String previousHash) {
        this.timestamp = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        this.transactionList = transactionList;
        this.idBlockForHash = idBlockForHash;
        this.previousHash = previousHash;
    }

    public Long getIdBlock() {
        return idBlock;
    }

    public void setIdBlock(Long idBlock) {
        this.idBlock = idBlock;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public BigInteger getNonce() {
        return nonce;
    }

    public void setNonce(BigInteger nonce) {
        this.nonce = nonce;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public long getIdBlockForHash() {
        return idBlockForHash;
    }

    public void setIdBlockForHash(long idBlockForHash) {
        this.idBlockForHash = idBlockForHash;
    }

    public boolean isBlockCorrectMined() {
        return blockCorrectMined;
    }

    public void setBlockCorrectMined(boolean blockCorrectMined) {
        this.blockCorrectMined = blockCorrectMined;
    }

    public String getHashed() throws NoSuchAlgorithmException {
        String hashedStringFromBlock = "";
        String transactionAreInString = "";

        for (Transaction transaction : transactionList) {
            transactionAreInString = transactionAreInString + transaction.toStringForMining();
        }

        hashedStringFromBlock = idBlockForHash + previousHash + transactionAreInString + timestamp + nonce;
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(hashedStringFromBlock.getBytes(StandardCharsets.UTF_8));
        return Utils.bytesToHex(hash).toUpperCase();
    }

    @Override
    public String toString() {
        return "Block{" +
                "idBlock=" + idBlock +
                ", idBlockForHash=" + idBlockForHash +
                ", previousHash='" + previousHash + '\'' +
                ", timestamp=" + timestamp +
                ", nonce=" + nonce +
                ", transactionList=" + transactionList +
                '}';
    }

    static Block getGenesis() {
        return new Block() {
            @Override
            public String getHashed() {
                return genesisBlockHash;
            }
            @Override
            public String getPreviousHash() {
                return genesisBlockHash;
            }
            @Override
            public BigInteger getNonce() {
                return BigInteger.valueOf(0000);
            }
        };
    }
}
