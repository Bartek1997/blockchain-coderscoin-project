package main.block;

import main.blockchainVisual.Node;
import main.transaction.Transaction;
import org.bitcoinj.core.SignatureDecodeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@CrossOrigin
@RestController
public class BlockController {
    @Autowired
    private BlockRepository blockRepository;
    @Autowired
    private BlockService blockService;

    @PostMapping("/auto-mine-block/{difficulty}/{rewardPublicKey}")
    public Block mineBlock(@PathVariable long difficulty, @PathVariable String rewardPublicKey, @RequestBody List<Transaction> transactionList) throws NoSuchAlgorithmException, SignatureDecodeException {
        return blockService.mineBlock(transactionList, difficulty, rewardPublicKey);
    }

    @PostMapping("/mine-block-with-nonce/{difficulty}/{rewardPublicKey}/{nonce}")
    public Block mineBlockWithNonce(@PathVariable long difficulty, @PathVariable String rewardPublicKey, @PathVariable BigInteger nonce, @RequestBody List<Transaction> transactionList) throws NoSuchAlgorithmException, SignatureDecodeException {
        return blockService.mineBlockWithNonce(transactionList, nonce, difficulty, rewardPublicKey);
    }

    @PostMapping("/save-block")
    public void saveNewBlock(@RequestBody List<Block> blockList) throws NoSuchAlgorithmException {
        blockService.saveBlocks(blockList);
    }

    @GetMapping("/get-longest-chain")
    public List<Block> getLongestChain() throws NoSuchAlgorithmException {
        return blockService.getLongestChainFromTheBlockchain();
    }

    @GetMapping("/get-blockchain")
    public Node getBlockchain() throws NoSuchAlgorithmException {
        return blockService.getBlockchain();
    }

    @GetMapping("/get-transactions-sorted-by-time")
    public List <Transaction> getTransByTime () throws NoSuchAlgorithmException {
        return blockService.getTransactionsByTime();
    }

    @GetMapping("/get-block-by-id/{idBlock}")
    public Block getBlockById (@PathVariable long idBlock){
        return blockRepository.findByIdBlock(idBlock);
    }

    @GetMapping("/get-all-blocks")
    public List<Block> completeBlockList(){
        return (List<Block>) blockRepository.findAll();
    }
}
