package main.keyPair;

import main.login.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KeyPrivatePublicRepository extends CrudRepository <KeyPrivatePublic, Long> {
    List<KeyPrivatePublic> findByUser(User user);
}
