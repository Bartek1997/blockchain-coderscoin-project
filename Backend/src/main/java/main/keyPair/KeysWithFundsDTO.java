package main.keyPair;

public class KeysWithFundsDTO {
    public String privateKeyWif;
    public String publicKeyBase58;
    public double amount;

    public KeysWithFundsDTO(String privateKeyWif, String publicKeyBase58, double amount) {
        this.privateKeyWif = privateKeyWif;
        this.publicKeyBase58 = publicKeyBase58;
        this.amount = amount;
    }

    public String getPrivateKeyWif() {
        return privateKeyWif;
    }

    public void setPrivateKeyWif(String privateKeyWif) {
        this.privateKeyWif = privateKeyWif;
    }

    public String getPublicKeyBase58() {
        return publicKeyBase58;
    }

    public void setPublicKeyBase58(String publicKeyBase58) {
        this.publicKeyBase58 = publicKeyBase58;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
