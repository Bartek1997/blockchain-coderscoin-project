package main.keyPair;

import main.block.BlockService;
import main.login.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


import java.security.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class KeyPrivatePublicController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private KeyPrivatePublicService keyPrivatePublicService;
    @Autowired
    private BlockService blockService;

    @GetMapping("/get-all-key-pairs-by-user")
    public List<KeyPrivatePublicDTO> getAllKeyPairsByUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        int idUser = userRepository.findByName(username).getIdUser();
        return keyPrivatePublicService.allKeyPairsByUser(idUser);
    }

    //request for generating a key pair
    @GetMapping("/key-pair-generator")
    public List<KeyPrivatePublicDTO> getKeyPairGenerator() throws NoSuchAlgorithmException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        int idUser = userRepository.findByName(username).getIdUser();
        KeyPrivatePublic keyPrivatePublic = keyPrivatePublicService.getKeyPairGenerator(idUser);
        return keyPrivatePublicService.keyPairList(keyPrivatePublic, idUser);
    }

    //request for entering manually a private key
    @GetMapping("/key-pair-manually/{wifPrivateKey}")
    public List<KeyPrivatePublicDTO> getKeyPairManually(@PathVariable String wifPrivateKey) throws NoSuchAlgorithmException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        int idUser = userRepository.findByName(username).getIdUser();
        return keyPrivatePublicService.keyPairManually(idUser, wifPrivateKey);
    }

    @GetMapping("/check-funds/{publicKey}")
    public double getFundsPerKeyPair (@PathVariable String publicKey) throws NoSuchAlgorithmException {
        return blockService.getFundsFromChain(publicKey, blockService.getLongestChainFromTheBlockchain());
    }

    @GetMapping("/get-key-pairs-with-funds")
    public List<KeysWithFundsDTO> getKeyPairsWithFunds () throws NoSuchAlgorithmException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        int idUser = userRepository.findByName(username).getIdUser();
        return keyPrivatePublicService.getKeysAndFunds(idUser);
    }

    @GetMapping("/get-funds-per-wallet")
    public double getFundsPerWallet () throws NoSuchAlgorithmException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        int idUser = userRepository.findByName(username).getIdUser();
        return keyPrivatePublicService.getFundsPerAccount(idUser);
    }

}
