package main.connectionVisual;

import java.util.List;

public class ConnectionVisual {
    private List<ConnectionVisualNode> nodes;
    private List<ConnectionVisualContent> edges;

    public ConnectionVisual(List<ConnectionVisualNode> nodesIds, List<ConnectionVisualContent> nodesContents) {
        this.nodes = nodesIds;
        this.edges = nodesContents;
    }

    public List<ConnectionVisualNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<ConnectionVisualNode> nodesIds) {
        this.nodes = nodesIds;
    }

    public List<ConnectionVisualContent> getEdges() {
        return edges;
    }

    public void setEdges(List<ConnectionVisualContent> edges) {
        this.edges = edges;
    }
}
