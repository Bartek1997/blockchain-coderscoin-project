package main.util;

import main.transaction.Transaction;

import java.security.MessageDigest;

public class Utils {
    public final static String genesisBlockHash = "0000000000000000000000000000000000000000000000000000000000000000";
    public final static String masterKeyPublic = "icHo5zXLvaUk8e2ak9kNU2tzMerQi3t2Jhyjenj12cFo";
    public final static String masterKeyPrivate = "5KV3KbkwHGgfKnWfpNFegQuE234imvLTuaQSD9ebA9EQgjVuiZ2";

    public static String calculateHash(Transaction transaction) {
        return applySha256(transaction.getPublicKeyWithdrawal()
                + transaction.getPublicKeyDeposit()
                + transaction.getAmount()
                + transaction.getTimestamp());
    }

    public static String applySha256(String input){
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            //Applies sha256 to our input,
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer(); // This will contain hash as hexidecimal
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static String adjustTo64(String s) {
        switch(s.length()) {
            case 62: return "00" + s;
            case 63: return "0" + s;
            case 64: return s;
            default:
                throw new IllegalArgumentException("not a valid key: "+ s);
        }
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte b : bytes) result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }
}
