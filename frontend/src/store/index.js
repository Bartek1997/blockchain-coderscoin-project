import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    users: [],
    blockchain: [],
  },
  getters: {
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setAllUsers(state, users) {
      state.users = users
    },
    setBlockchain(state, blockchain){
      state.blockchain = blockchain
    },
  },
  actions: {
    //File handling
    async fileUpload(store, file){
      await axios.post('upload/', file)
    },

    //Transaction
    async createTransaction(store, transaction){
      await axios.post('create-transaction/' + transaction.signKey , transaction)
    },

    //Mining
    async mineBlock(store, mPathVariables){
      await axios.post('auto-mine-block/' +mPathVariables.difficulty +'/' +mPathVariables.rewardKey, mPathVariables.file)
    },
    async mineBlockManuelly(store, mPathVariables){
      await axios.post('mine-block-with-nonce/' +mPathVariables.difficulty +'/' +mPathVariables.rewardKey +'/' +mPathVariables.nonce, mPathVariables.file)
    },

    //Wallet
    async generateKey(){
      await axios.get('user/key-pair-generator')
    },
    async addKey(store, wifPrivateKey){
      await axios.get('user/key-pair-manually/' +wifPrivateKey)
    },

    //Login, Register & Logout
    async login (context, credentials) {
      var basicAuth = 'Basic ' + btoa(credentials.username + ':' +credentials.password)
      const response = await Vue.axios.get('/api/signedInUser', {
        headers: { 'Authorization': basicAuth }
      })
      context.commit('setUser', response.data)
    },
    async register (context, newUserData) {
      await Vue.axios.post('/api/register', newUserData)
    },
    async getSignedInUser (context) {
      const response = await Vue.axios.get('/api/signedInUser')
      context.commit('setUser', response.data)
    },
    async logout (context) {
      await Vue.axios.post('/api/logout')
      context.commit('setUser', null)
    },
    async loadAllUsers(context) {
      const response = await Vue.axios.get('/api/users')
      context.commit('setAllUsers', response.data)
    },

    //Blockchain
    async getBlockchain(store){
      const userResponse = await axios.get('get-blockchain/')
      store.commit('setBlockchain', userResponse.data)
    },
    /* //Longest chain in the blockchain
    async getLongestChain(store){
      const userResponse = await axios.get('get-longest-chain/')
      store.commit('setLongestChain', userResponse.data)
    },
    //Transactions sorted by time
    async getTransactionsSortedByTime(store){
      const userResponse = await axios.get('get-transactions-sorted-by-time/')
      store.commit('setTransactionsSortedByTime', userResponse.data)
    }, */

  },
  modules: {
  }
})
