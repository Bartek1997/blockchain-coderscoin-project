import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import store from './store'
//import * as THREE from 'three';
//import * as VueGL from 'vue-gl';

//Object.entries(VueGL).forEach(([name, component]) => Vue.component(name, component));
//Vue.component('THREE', THREE);
Vue.use(VueAxios, axios)


Vue.config.productionTip = false


async function createVueApp() {
  await store.dispatch('getSignedInUser')
  new Vue({
    router,
    store,
    render: h => h(App),
    // components: VueGL,
    // el: "#canvas"
  }).$mount('#app')
}
createVueApp()