import Vue from 'vue'
import VueRouter from 'vue-router'
import LandingPage from '../views/LandingPageView'
import Dashboard from '../views/DashboardView'
import Wallet from '../views/WalletPageView'
import Login from '../views/LoginView'
import Register from '../views/RegisterView'
import Mining from '../views/MiningView'
import Transaction from '../views/TransactionView'
import Blockchain from '../views/BlockchainView'
import BlockDetails from '../views/DetailBlockView'
import TransactionDetails from '../views/DetailTransactionView'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: "/", component: LandingPage, meta:{hideNavBar: true}
  },
  {
    path: "/dashboard", component: Dashboard
  },
  {
    path: "/wallet", component: Wallet,
      beforeEnter (to, from, next) {
      if (store.state.user) {
        return next()
      }
      return next('/login')
    } 
  },
  {
    path: "/login", component: Login, meta:{hideNavBar: true},
    beforeEnter (to, from, next) {
      if(store.state.user) {
        return next('/wallet')
      }
      return next()
    }
  },
  {
    path: "/register", component: Register, meta:{hideNavBar: true}
  },
  {
    path: "/mining", component: Mining
  },
  {
    path: "/transaction", component: Transaction
  },
  {
    path: "/blockchain", component: Blockchain
  },
  {
    path: "/block-details", component: BlockDetails, name: "BlockDetail"
  },
  {
    path: "/transaction-details", component: TransactionDetails, name: "TransactionDetail"
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
